let Application = async (settings = null) => {
    /**
     * App
     * ~ Organize app data and functions
     */
    let App = {
        /**
         * Settings for the app
         */
        Settings: null,

        /**
         * Handle app navigation
         */
        Navigation: {
            /**
             * Keep track of current navigation for the app
             */
            Current: {
                menu: null,
                content: null
            },

            /**
             * Switch Viewable Screen
             */
            navigate: (element = null) => {
                let page = element.getAttribute('data-page');

                /* Remove active class from current page */
                App.Navigation.Current.content.classList.remove('active');
                App.Navigation.Current.menu.classList.remove('active');

                /* Switch to new page */
                App.Navigation.Current.menu = element;
                App.Navigation.Current.menu.classList.add('active');
                App.Navigation.Current.content = document.querySelector(`#${page}`);
                App.Navigation.Current.content.classList.add('active');
            }
        },

        /**
         * Utility functions for the app
         */
        Utility: {
            /*
             * Make http request and return the results
             */
            fetch: async (method = 'POST', route = null, data = null) => {
                return fetch(
                    App.Settings.Api.url + route, 
                    { 
                        method: method,
                        headers: { 'Content-Type': 'application/json' }, 
                        body: JSON.stringify(data)
                    }
                ).then(response => response.json());
            },

            /* Handle loader */
            Loader: {
                /* Run the loader for x miliseconds */
                run: (time = 1000) => {
                    App.Utility.Loader.start();

                    /* stop the loader after time */
                    setTimeout(App.Utility.Loader.stop, time);
                },

                /* Start the loader */
                start: () => {
                    document.querySelector('#loader').classList.add('active');
                },

                /* Stop the loader */
                stop: () => {
                    document.querySelector('#loader').classList.remove('active');
                }
            },

            /**
             * Add a status message to the page
             */
            dialog: (message = null, type = 'success', time = 6000) => {
                let dialog = document.querySelector('#dialog');

                /* switch dialog type */
                switch (type) {
                    case 'error':
                        dialog.classList.add('error');
                        dialog.classList.remove('success');
                        dialog.classList.remove('warn');
                        break;
                    case 'warn':
                        dialog.classList.add('warn');
                        dialog.classList.remove('success');
                        dialog.classList.remove('error');
                        break;
                    case 'success':
                    default: 
                        dialog.classList.add('success');
                        dialog.classList.remove('error');
                        dialog.classList.remove('warn');
                }

                /* updae message and show the dialog */
                dialog.innerHTML = message;
                dialog.classList.add('active');

                /* remove dialog after 6 seconds */
                setTimeout(() => { dialog.classList.remove('active'); }, time);
            },

            /**
             * Log data to the console when debug mode is active
             */
            log: (data) => {
                if (App.Settings.debug) {
                    console.log(data);
                }
            },

            /**
             * Wait for X Time
             */
            wait: (delay, ...args) => new Promise(resolve => setTimeout(resolve, delay, ...args))
        }
    };

    /**
     * Initialize the App
     */
    (async () => {
        /* Add settings to the app */
        App.Settings = settings;
        settings = null;

        App.Utility.log('Initializing Adon');

        /* Wait a few seconds to load resources */
        await App.Utility.wait(1000);

        /* Add listeners to the App Links */
        document.querySelectorAll('#app .app-link').forEach(element => {
            element.addEventListener('click', (menu) => {
                App.Navigation.navigate(element);
            });
        });

        /* Set current active page */
        App.Navigation.Current.menu = document.querySelector('#app .app-link.active');
        App.Navigation.Current.content = document.querySelector('#app .page.active');

        /* Remove the splash screen */
        document.querySelector('#splash').classList.remove('active');

        /* Display the app */
        document.querySelector('#app').classList.add('active');

        App.Utility.log('Ending Initialization');
    })();
}